const express = require("express");
import app from "./app";

const port = process.env.PORT || 1338;
app.listen(port, () => {
  console.log(`App corriendo en el puerto ${port}!`);
});
