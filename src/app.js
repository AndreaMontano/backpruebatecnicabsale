import express from "express";
import "core-js/stable";
import "regenerator-runtime/runtime";
import cors from "cors";
import Page from "./page/routes";

//Connection to database first
require("./database");

const methodOverride = require("method-override");
const isProduction = process.env.NODE_ENV === "production";

//Initializate express
const app = express();

//When we use delete and put method that aren't supported by the client
app.use(methodOverride("_method"));
//Used to recognize the incoming request object as a json
app.use(express.json());

// To receive requests sent from the production mode
// app.use(
//   cors({ credentials: true, origin: "https://bsalestore.herokuapp.com" })
// );
// To receive requests sent from the development mode
app.use(cors({ credentials: true, origin: "http://localhost:3000" }));

//routes
app.use("/api/page", Page.public);

app.get("/", (req, res) => {
  res.status(200).send("My Web Page- GAMV");
});

if (!isProduction) {
  app.use(function (err, req, res, next) {
    console.log(err.stack);

    res.status(err.status || 500);

    res.json({
      errors: {
        message: err.message,
        error: err,
      },
    });
  });
}

app.use((req, res, next) => {
  const err = new Error("Not Found");
  err.status = 404;
  next(err);
});

app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    errors: {
      message: err.message,
      error: {},
    },
  });
});

export default app;
