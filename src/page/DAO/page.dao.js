//We import the connection to the database
import { connection } from "../../database";

class PageDao {
  //we receive a string with the query to the database and the searched word
  askingDataBase = (sql, word) => {
    const allProducts = new Promise((resolve, reject) => {
      //we establish separately the query and the parameters as a method to prevent SQL injections
      connection.query(sql, [`%${word}%`], (err, elements) => {
        if (err) {
          return reject(err);
        }
        return resolve(elements);
      });
    });
    return allProducts;
  };
}

export default PageDao;
