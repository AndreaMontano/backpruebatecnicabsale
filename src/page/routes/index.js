import express from "express";
import PageController from "../controllers/index";

const publicRoute = express.Router();

// As we dont have user authentication in this application, it's not necessary to establish any private route
publicRoute.get("/get-all-products", async (req, res) => {
  try {
    //We make the request to the controller to bring all the products
    const allProducts = await PageController.getAllProducts();
    //We answer an array with all the products
    res.json(allProducts);
  } catch (error) {
    res.status(400);
  }
});

publicRoute.get("/search-products/:word", async (req, res) => {
  //We receive by parameter the word to be searched
  const word = req.params.word;

  try {
    //e make the request to the controller to bring the products that match the search word
    const searchProducts = await PageController.searchProducts(word);
    res.json(searchProducts);
  } catch (error) {
    res.status(400);
  }
});

export default {
  public: publicRoute,
};
