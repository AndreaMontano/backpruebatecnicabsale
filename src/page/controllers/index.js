import DAO from "../DAO/page.dao";

const dao = new DAO();

//We create the query to the database as a constant
const queries = {
  allProducts: "SELECT * FROM bsale_test.product",
  allCategories: "SELECT * FROM bsale_test.category",
  selectedProducts: "SELECT * FROM bsale_test.product WHERE name LIKE ?",
};

//We make the asynchronous request to the database and say wait for the response
const getAllProducts = async () => {
  const getAllProducts = await dao.askingDataBase(queries.allProducts);
  const getAllCategories = await dao.askingDataBase(queries.allCategories);

  //We return an object with the array of products and the array of categories
  return await {
    getAllProducts,
    getAllCategories,
  };
};

const searchProducts = async (word) => {
  const getAllProducts = await dao.askingDataBase(queries.allProducts);
  const getSelectedProducts = await dao.askingDataBase(
    // Estoy usando el método de Instrucciones SQL prepaadas para prevenir SQL Injections. Usamos "?" y mandamos como parámetro el texto a buscar, esto impide que se ejecuten instrucciones adicionales.
    queries.selectedProducts,
    word
  );
  const getAllCategories = await dao.askingDataBase(queries.allCategories);

  return await {
    getAllProducts,
    getSelectedProducts,
    getAllCategories,
  };
};

export default {
  getAllProducts,
  searchProducts,
};
